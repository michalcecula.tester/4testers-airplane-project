from src.airplane import Airplane
from src.airline import Airline
import pytest

@pytest.fixture
def new_airplane_airbus():
    return Airplane("Airbus", 200)


@pytest.fixture
def new_airplane_boeing():
    return Airplane("Boeing", 200)

@pytest.fixture
def airline_with_single_airplane(new_airplane_airbus):
    return Airline([new_airplane_airbus])

@pytest.fixture
def airline_with_two_airplanes(new_airplane_boeing, new_airplane_airbus):
    return Airline([new_airplane_airbus, new_airplane_boeing])