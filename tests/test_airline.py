from src.airline import Airline

def test_create_new_airline_and_check_initial_properties(new_airplane_airbus, new_airplane_boeing):
    airline = Airline([new_airplane_airbus, new_airplane_boeing])
    assert airline.get_available_seats_in_airplanes() == 400

def test_airline_with_one_airplane(airline_with_single_airplane):
    assert len(airline_with_single_airplane.airplanes) == 1

def test_airline_with_two_airplanes(airline_with_two_airplanes):
    assert len(airline_with_two_airplanes.airplanes) == 2