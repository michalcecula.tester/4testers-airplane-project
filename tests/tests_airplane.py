import pytest
from src.airplane import Airplane
from assertpy import assert_that


# @pytest.fixture
# def new_airplane():
#     return Airplane("Airbus", 200)
def test_new_airplane_is_type_of_airplane():
    new_airplane = Airplane("Airbus", 200)
    assert_that(new_airplane).is_type_of(Airplane)


def test_mileage_is_0_and_seats_taken_is_0():
    new_airplane = Airplane("Airbus", 200)
    assert_that(new_airplane.taken_seats and new_airplane.mileage).is_equal_to(0)


def test_fly_5000_km():
    new_airplane = Airplane("Airbus", 200)
    new_airplane.fly(5000)
    assert_that(new_airplane.mileage).is_equal_to(5000)


def test_fly_5000_and_3000_km():
    new_airplane = Airplane("Airbus", 200)
    new_airplane.fly(5000)
    new_airplane.fly(3000)
    assert_that(new_airplane.mileage).is_equal_to(8000)


def test_is_service_required_for_9999_km():
    new_airplane = Airplane("Airbus", 200)
    new_airplane.fly(9999)
    new_airplane.is_service_required()
    assert_that(new_airplane.is_service_required()).is_equal_to(False)


def test_is_service_required_for_10001_km():
    new_airplane = Airplane("Airbus", 200)
    new_airplane.fly(10001)
    new_airplane.is_service_required()
    assert_that(new_airplane.is_service_required()).is_equal_to(True)


def test_available_seats_after_180_boarded_passengers():
    new_airplane = Airplane("Airbus", 200)
    new_airplane.board_passengers(180)
    assert_that(new_airplane.get_available_seats()).is_equal_to(20)


def test_boarding_passengers_over_seats_limit():
    new_airplane = Airplane("Airbus", 200)
    new_airplane.board_passengers(180)
    assert_that(new_airplane.get_available_seats()).is_equal_to(20)
