class Airplane:
    def __init__(self, name, number_of_seats):
        self.name = name
        self.number_of_seats = number_of_seats
        self.mileage = 0
        self.taken_seats = 0

    def fly(self, distance):
        self.mileage += distance

    def is_service_required(self):
        return self.mileage > 10000

    def board_passengers(self, number_of_passengers):
        if number_of_passengers <= self.get_available_seats():
            self.taken_seats += number_of_passengers
        else:
            self.taken_seats = self.number_of_seats

    def get_available_seats(self):
        return self.number_of_seats - self.taken_seats

    def __str__(self):
        return (
            f"Samolot nazwa: {self.name},"
            f" ilość miejsc: {self.number_of_seats},"
            f" zajęte miejsca: {self.taken_seats},"
            f'przebieg: {self.mileage}'
        )


if __name__ == '__main__':
    plane_1 = Airplane("Boeing", 200)
    plane_2 = Airplane("Airbus", 200)

    print(plane_1)
    print(plane_2)

    print(plane_1.name, plane_1.number_of_seats)
    plane_1.fly(10000)
    print(plane_1.is_service_required())
    plane_1.board_passengers(100)
    plane_1.get_available_seats()
    print(plane_1.get_available_seats())

    print(plane_2.name, plane_2.number_of_seats)
    plane_2.fly(11000)
    print(plane_2.is_service_required())
    plane_2.board_passengers(199)
    plane_2.get_available_seats()
    print(plane_2.get_available_seats())
