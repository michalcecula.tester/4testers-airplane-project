# Klasa Airline opisuje linie lotnicze. Linia lotnicza w trakcie
# inicjalizacji przyjmuje listę samolotów.

# W klasie została napisana metoda zwracająca ilość wszystkich
# dostępnych miejsc we wszystkich samolotach.

from src.airplane import Airplane


class Airline:
    def __init__(self, airplanes: [Airplane]):
        self.airplanes = airplanes

    def __str__(self):
        return f"Lista samolotów: {self.airplanes[0]}"

    def get_available_seats_in_airplanes(self):
        all_seats = 0
        for airplane in self.airplanes:
            all_seats += airplane.get_available_seats()
        return all_seats


if __name__ == '__main__':
    airplane_1 = Airplane("Airbus", 200)
    airplane_2 = Airplane("Boeing", 200)
    airplane_2.board_passengers(50)
    airplanes = [airplane_1, airplane_2]
    airline = Airline(airplanes)
    print(airline.get_available_seats_in_airplanes())
